import {Component, OnInit} from '@angular/core';
import {CarBrandsService} from "../../../../services/car-brands/car-brands.service";
import {CarBrand} from "../../../../data/car_brand";
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
	selector: 'app-quotation-form',
	templateUrl: './quotation-form.component.html',
	styleUrls: ['./quotation-form.component.scss']
})
export class QuotationFormComponent implements OnInit {

	public years: Array<number>;

	public carBrands: Array<CarBrand>;
	public carBrandModels: CarBrand;

	public formGroup: FormGroup;




	constructor(private carBrandsService: CarBrandsService) {
		this.years = [];
		this.carBrands = [];
		this.carBrandModels = {};

		this.formGroup = new FormGroup({
			car_brand: new FormControl(null, Validators.required),
			car_model: new FormControl(null, Validators.required),
			car_year: new FormControl(null, Validators.required),
			car_email: new FormControl(null, [Validators.required, Validators.email])
		});


		this.formGroup.get('car_brand')?.valueChanges.subscribe(response => {
			this.onChangeCarBrand();

			if (this.disabledSelectModels()) {
				this.formGroup.get('car_model')?.disable();
			}
		});


		this.formGroup.get('car_model')?.disable();
	}




	ngOnInit(): void {
		this.chargeYears();
		this.getCarBrands();
	}




	private chargeYears() {
		try {

			const currentYear: number = (new Date()).getFullYear();

			if (currentYear) {
				for (let year = currentYear; year > 1938; year--) {
					this.years.push(year);
				}
			}

		} catch (e) {
			console.error(e);
		}
	}



	private getCarBrands() {
		try {

			this.carBrandsService.getCarBrands()
				.then(response => {
					if (response) {
						this.carBrands = response;
					}
				})
				.catch(error => {
					console.error(error);
				});

		} catch (e) {
			console.error(e);
		}
	}



	public onChangeCarBrand(): void {
		try {

			this.formGroup.get('car_model')?.setValue(null);

			const carBrandId = this.formGroup.get('car_brand')?.value;

			if (carBrandId && (carBrandId !== '')) {

				this.carBrandsService.getModelsByIdBrand(carBrandId)
					.then(response => {
						if (response) {
							this.carBrandModels = response;

							this.formGroup.get('car_model')?.enable();
						}
					})
					.catch(error => {
						console.error(error);

						this.carBrandModels = {};
					});

			} else {

				this.carBrandModels = {};
			}

		} catch (e) {
			console.error(e);
		}
	}



	public disabledSelectModels(): boolean {
		try {

			return (!this.carBrandModels || !this.carBrandModels['models'] || (this.carBrandModels['models'].length === 0));

		} catch (e) {
			console.error(e);
		}


		return true;
	}

}
