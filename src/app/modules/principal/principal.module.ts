import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PrincipalRoutingModule} from './principal-routing.module';
import {PrincipalComponent} from './principal.component';
import {QuotationFormComponent} from './components/quotation-form/quotation-form.component';
import {CarBrandsService} from "../../services/car-brands/car-brands.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";


@NgModule({
	declarations: [
		PrincipalComponent,
		QuotationFormComponent
	],
	imports: [
		CommonModule,
		PrincipalRoutingModule,
		FormsModule,
		ReactiveFormsModule
	],
	providers: [
		CarBrandsService
	]
})
export class PrincipalModule {
}
