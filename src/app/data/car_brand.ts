import {CarModel} from "./car_model";

export interface CarBrand {
	id?: string;
	brand?: string;
	models?: Array<CarModel>;
};
