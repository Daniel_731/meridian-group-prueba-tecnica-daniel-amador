import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {CarBrand} from "../../data/car_brand";

declare const require: any;

@Injectable({
	providedIn: 'root'
})
export class CarBrandsService {

	private axios;



	constructor() {
		this.axios = require('axios').default;
	}




	public async getCarBrands(): Promise<Array<CarBrand>> {
		try {

			return new Promise((resolve, reject) => {
				const URL = 'https://b39a7859-1749-4d86-aa8c-25de832e78a1.mock.pstmn.io/carbrands';

				this.axios.get(URL, {})
					.then((response: { [x: string]: CarBrand[] | PromiseLike<CarBrand[]>; }) => {
						resolve(response[`data`]);
					})
					.catch((error: object) => {
						reject(error);
					});
			});

		} catch (e) {
			console.error(e);
		}

		return [];
	}



	public async getModelsByIdBrand(idCarBrand: string): Promise<CarBrand> {
		try {

			return new Promise((resolve, reject) => {
				const URL = `https://b39a7859-1749-4d86-aa8c-25de832e78a1.mock.pstmn.io/carbrands/${idCarBrand}`;

				this.axios.get(URL, {})
					.then((response: { [x: string]: CarBrand | PromiseLike<CarBrand>; }) => {
						resolve(response[`data`]);
					})
					.catch((error: object) => {
						reject(error);
					});
			});

		} catch (e) {
			console.error(e);
		}

		return {};
	}

}
